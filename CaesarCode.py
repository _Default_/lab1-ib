alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"

def encode(strForEncode, key):
    encodeStr = ''
    for smb in strForEncode:
        leterIndex = alphabet.find(smb) 
        if leterIndex == -1:
            encodeStr += smb
        else:
            encodeStr += alphabet[leterIndex + key - len(alphabet)]
    return encodeStr


def decode(strForDecode, key):
    decodeStr = ''
    for smb in strForDecode:
        leterIndex = alphabet.find(smb) 
        if leterIndex == -1:
            decodeStr += smb
        else:
            decodeStr += alphabet[leterIndex - key]
    return decodeStr


