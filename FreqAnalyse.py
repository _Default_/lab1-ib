alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"

def generateProcentRateFromFreqDict(freqDict):
    textLen = sum(freqDict.values())
    procentRate = [(smb, qty / textLen) for smb, qty in freqDict.items()]
    return procentRate


def oneSymbolAnalyze(text):
    freqDictionary = {smb:0 for smb in alphabet}

    for smb in text:
        if smb in alphabet:
            freqDictionary[smb] += 1
        
    return generateProcentRateFromFreqDict(freqDictionary)

def bigramAnalyze(text):
    freqDictionary = {}

    for i in range(len(text) - 1):
        bigram = text[i] + text[i+1]
        if bigram[0] not in alphabet and bigram[1] not in alphabet:
            continue

        if bigram in freqDictionary:
            freqDictionary[bigram] += 1
        else:
            freqDictionary[bigram] = 1

    return generateProcentRateFromFreqDict(freqDictionary)