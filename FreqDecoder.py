import FreqAnalyse


def find(items, val):
    index = -1
    for i in range(len(items)):
        if items[i][0] == val:
            index = i
            break
    return index


def correctRates(encodedText, text, encodedFileRate, textFileRate, bigramQty=5):
    bigramsRateEncoded = FreqAnalyse.bigramAnalyze(encodedText)
    bigramsRateText = FreqAnalyse.bigramAnalyze(text)

    bigramsRateEncoded.sort(key=lambda elem: elem[1], reverse=True)
    bigramsRateText.sort(key=lambda elem: elem[1], reverse=True)

    for i in range(len(bigramsRateEncoded[0:bigramQty])):
        bigramEnc = bigramsRateEncoded[i][0]
        bigramText = bigramsRateText[i][0]

        for j in range(len(bigramEnc)):
            smbEnc, smbText = bigramEnc[j], bigramText[j]
            if smbEnc not in FreqAnalyse.alphabet or smbText not in FreqAnalyse.alphabet:
                continue

            index = find(encodedFileRate, smbEnc)
            if textFileRate[index][0] != smbText:
                textIndex = find(textFileRate, smbText)
                textFileRate[index], textFileRate[textIndex] = textFileRate[textIndex], textFileRate[index]


def decode(encodedPath, textPath, useBigrams=False):
    with open(encodedPath, encoding='utf-8') as file:
        encodedText = file.read().lower()
        encodedFileRate = FreqAnalyse.oneSymbolAnalyze(encodedText)

    with open(textPath, encoding='utf-8') as file:
        text = file.read().lower()
        textFileRate = FreqAnalyse.oneSymbolAnalyze(text)

    encodedFileRate.sort(key=lambda elem: elem[1], reverse=True)
    textFileRate.sort(key=lambda elem: elem[1], reverse=True)

    if useBigrams:
        correctRates(encodedText, text, encodedFileRate, textFileRate)

    for i in range(len(encodedFileRate)):
        print(encodedFileRate[i], textFileRate[i])

    decodedText = ''
    for smb in encodedText:
        letterIndex = find(encodedFileRate, smb)
        if letterIndex == -1:
            decodedText += smb
        else:
            decodedText += textFileRate[letterIndex][0]

    return decodedText
