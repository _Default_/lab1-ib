import CaesarCode
import FreqDecoder
import FreqAnalyse

from os.path import join

key = 2
folderName = 'Files'

# Кодирование главы шифром цезаря
encodeChaper = open(join(folderName, 'encodeChapter.txt'), 'w', encoding='utf-8')
with open(join(folderName, 'Chapter.txt'), encoding='utf-8') as chapter:
    for line in chapter:
        encodeStr = CaesarCode.encode(line.lower(), key)
        encodeChaper.write(encodeStr)
encodeChaper.close()

# раскодировать с помощью частотного анализа по символу
decodedText = FreqDecoder.decode(join(folderName, 'encodeChapter.txt'), join(folderName, 'Book.txt'))
with open(join(folderName, 'outOneSmb.txt'), 'w', encoding='utf-8') as file:
    file.write(decodedText)

# раскодировать с помощью частотного анализа по биграммам
decodedText = FreqDecoder.decode(join(folderName, 'encodeChapter.txt'), join(folderName, 'Book.txt'), True)
with open(join(folderName, 'outBigram.txt'), 'w', encoding='utf-8') as file:
    file.write(decodedText)




    